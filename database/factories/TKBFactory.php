<?php

namespace Database\Factories;

use App\Models\TKB;
use Illuminate\Database\Eloquent\Factories\Factory;

class TKBFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TKB::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
