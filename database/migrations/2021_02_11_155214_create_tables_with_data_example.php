<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablesWithDataExample extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('User Name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('role', ['admin', 'customer'])->comment('Role của user');
            $table->rememberToken();

            $table->timestamps();
        });

        Schema::create('truong_hocs', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('Tên Trường');

            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('tkb', function (Blueprint $table) {
            $table->id();
            $table->integer('hoc_ky');
            $table->integer('nam_hoc');
            $table->unsignedBigInteger('truong_hoc_id');

            $table->foreign('truong_hoc_id')
                ->references('id')
                ->on('truong_hocs')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('giao_viens', function (Blueprint $table) {
            $table->id();
            $table->string('code')->comment('Mã gv');
            $table->string('name');
            $table->date('birthday')->nullable();
            $table->enum('gioi_tinh', ['nu', 'nam'])->comment('Giới tính');
            $table->string('phone', 20)->nullable();
            $table->string('email', 100)->nullable();
            $table->longText('address')->nullable()->comment('Địa chỉ');

            $table->unsignedBigInteger('tkb_id');

            $table->foreign('tkb_id')
                ->references('id')
                ->on('tkb')
                ->onDelete('cascade');

        });

        Schema::create('mon_hocs', function (Blueprint $table) {
            $table->id();
            $table->string('code')->comment('Mã môn');
            $table->string('name');

            $table->integer('so_phong_mon')->comment('Số phòng môn');
            $table->integer('he_so')->comment('Hệ số');


            $table->unsignedBigInteger('tkb_id');

            $table->foreign('tkb_id')
                ->references('id')
                ->on('tkb')
                ->onDelete('cascade');
        });

        Schema::create('lop_hocs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('location')->comment('Vị trí');
            $table->integer('tong_so_hs')->nullable();
            $table->enum('chuong_trinh_dt', ['co ban', 'nang cao'])->comment('Chương trình ĐT');
            $table->enum('phong_tt', ['co', 'khong'])->comment('Phòng TT');
            $table->enum('buoi', ['sang', 'chieu'])->comment('Buổi');

            $table->unsignedBigInteger('tkb_id');

            $table->foreign('tkb_id')
                ->references('id')
                ->on('tkb')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('giao_viens');
        Schema::dropIfExists('mon_hocs');
        Schema::dropIfExists('lop_hocs');
        Schema::dropIfExists('tkb');
        Schema::dropIfExists('truong_hocs');
        Schema::dropIfExists('users');
    }
}
