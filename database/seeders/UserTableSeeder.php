<?php

namespace Database\Seeders;

use Flynsarmy\CsvSeeder\CsvSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends CsvSeeder
{

    public function __construct()
    {
        $this->table = 'users';
        $this->filename = base_path() . '/database/seeds/csvs/user.csv';
        $this->hashable = ['password'];
        $this->timestamps = true;
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();
        parent::run();
    }
}
