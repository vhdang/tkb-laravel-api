<?php

namespace Database\Seeders;

use Flynsarmy\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class LopHocTableSeeder extends CsvSeeder
{

    public function __construct()
    {
        $this->table = 'lop_hocs';
        $this->filename = base_path() . '/database/seeds/csvs/lophoc.csv';
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();
        parent::run();
    }
}
