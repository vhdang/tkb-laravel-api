<?php

namespace Database\Seeders;

use Flynsarmy\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class GiaoVienTableSeeder extends CsvSeeder
{

    public function __construct()
    {
        $this->table = 'giao_viens';
        $this->filename = base_path() . '/database/seeds/csvs/giaovien.csv';
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();
        parent::run();
    }
}
