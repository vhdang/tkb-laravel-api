<?php

namespace Database\Seeders;

use Flynsarmy\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class TKBTableSeeder extends CsvSeeder
{

    public function __construct()
    {
        $this->table = 'tkb';
        $this->filename = base_path() . '/database/seeds/csvs/tkb.csv';
        $this->timestamps = true;
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();
        parent::run();
    }
}
