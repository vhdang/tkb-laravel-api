<?php

namespace Database\Seeders;

use Flynsarmy\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class MonHocTableSeeder extends CsvSeeder
{

    public function __construct()
    {
        $this->table = 'mon_hocs';
        $this->filename = base_path() . '/database/seeds/csvs/monhoc.csv';
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();
        parent::run();
    }
}
