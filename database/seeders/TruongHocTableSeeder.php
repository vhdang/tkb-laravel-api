<?php

namespace Database\Seeders;

use Flynsarmy\CsvSeeder\CsvSeeder;
use Illuminate\Support\Facades\DB;

class TruongHocTableSeeder extends CsvSeeder
{

    public function __construct()
    {
        $this->table = 'truong_hocs';
        $this->filename = base_path() . '/database/seeds/csvs/truonghoc.csv';
        $this->timestamps = true;
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();
        parent::run();
    }
}
