<?php

namespace Database\Seeders;

use App\Http\Controllers\MonHocController;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
                UserTableSeeder::class,
                TruongHocTableSeeder::class,
                TKBTableSeeder::class,
                GiaoVienTableSeeder::class,
                LopHocTableSeeder::class,
                MonHocTableSeeder::class
            ]
        );
    }
}
