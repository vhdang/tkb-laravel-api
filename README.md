TKB 12
==========

HOW TO RUN LOCAL:
-------------
```
cp .env.local .env
composer install
sail up -d
sail artisan migrate:fresh --seed
```
