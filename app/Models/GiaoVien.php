<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GiaoVien extends Model
{
    use HasFactory;

    protected $table = 'giao_viens';

    protected $fillable = ['code', 'name', 'gioi_tinh', 'birthday', 'tkb_id'];
    public $timestamps = false;
}
