<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function truongHocs()
    {
        return $this->hasMany(TruongHoc::class, 'user_id');
    }

    /**
     * Add custom attributes when get via API
     *
     * @return array
     */
    public function toArray()
    {
        $array = parent::toArray();
        $array['tkbs'] = $this->tkbs()->get();
        return $array;
    }

    public function tkbs()
    {
        $truongHocs = TruongHoc::where('user_id', $this->id)->get('id')->toArray();
        $tkbs = TKB::whereIn('truong_hoc_id', $truongHocs);
        return $tkbs;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
