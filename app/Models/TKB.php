<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TKB extends Model
{
    use HasFactory;

    protected $table = 'tkb';

    public function truongHoc()
    {
        return $this->hasOne(TruongHoc::class, 'id', 'truong_hoc_id');
    }

    /**
     * Add custom attributes when get via API
     *
     * @return array
     */
    public function toArray()
    {
        $array = parent::toArray();
        $array['name'] = $this->getNameAttribute();
        return $array;
    }

    public function getNameAttribute()
    {
        $name = $this->truongHoc()->get('name');

        return $name[0]['name'] . ' _ HK ' . $this->hoc_ky . ' _ ' . $this->nam_hoc . '-' . $this->nam_hoc++;
    }
}
