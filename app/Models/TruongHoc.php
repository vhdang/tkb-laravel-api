<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TruongHoc extends Model
{
    use HasFactory;

    protected $table = 'truong_hocs';


    public function tkbs()
    {
        return $this->hasMany(TKB::class, 'truong_hoc_id');
    }
}
