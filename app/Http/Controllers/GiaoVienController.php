<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetInfoViaTKBRequest;
use App\Models\GiaoVien;
use Illuminate\Http\Request;

class GiaoVienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GetInfoViaTKBRequest $request)
    {
//        $user = auth('api')->user();
        return GiaoVien::where('tkb_id', $request->tkb_id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\GiaoVien $giaoVien
     * @return \Illuminate\Http\Response
     */
    public function show(GiaoVien $giaoVien)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\GiaoVien $giaoVien
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GiaoVien $giaoVien)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\GiaoVien $giaoVien
     * @return \Illuminate\Http\Response
     */
    public function destroy(GiaoVien $giaoVien)
    {
        //
    }
}
